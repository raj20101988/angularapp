import { NgModule } from '@angular/core';
import {RouterModule, Route, Routes} from '@angular/router';
import {NotFoundComponent} from "../authentication/components/not-found/not-found.component";
import {UserprofileComponent} from "./components/userprofile/userprofile.component";
import { DashboardComponent } from './dashboard.component';
import { UsergalleryComponent } from './components/usergallery/usergallery.component';

const dashboardRoutes: Routes = [
  { path: '', 
  component: DashboardComponent ,
  children: [
    { path: 'user-profile', component:UserprofileComponent},
    //{ path: 'user-profile', component:UserprofileComponent},
    { path: 'gallery', component:UsergalleryComponent},
    { path: '',   redirectTo: 'user-profile', pathMatch: 'full' },
    //{ path: '**', component:NotFoundComponent }
  ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(dashboardRoutes)
  ],
  exports:[RouterModule]
})
export class DashboardRoutingModule { }