import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import { MaterialComponentModule } from '../app.module';
import {RouterTestingModule} from "@angular/router/testing";
import {Router, RouterLinkWithHref, Routes} from "@angular/router";
import {UserprofileComponent} from "../dashboard/components/userprofile/userprofile.component";
import {Component, DebugElement, NgModule, Type} from "@angular/core";
import { UsergalleryComponent } from "../dashboard/components/usergallery/usergallery.component";
import {SpyLocation} from "@angular/common/testing";
import {By} from "@angular/platform-browser";
import {RouterLinkDirectiveStub, RouterStubsModule} from "../../testing/router-link-directive-stub";
import {Location} from "@angular/common";
import { UsersService } from '../services/users.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@Component({ template: '' })
class MockUserprofileComponent {}
@Component({ template: '' })
class MockUsergalleryComponent {}

@NgModule({
  declarations: [MockUserprofileComponent,MockUsergalleryComponent],
  exports:      [MockUserprofileComponent,MockUsergalleryComponent]
})
class MockDashboardModule { }

let component: DashboardComponent;
let fixture: ComponentFixture<DashboardComponent>;
let usersService:UsersService;
let location:Location;
let router:Router
let page:Page
let routerLinks:RouterLinkDirectiveStub[];
let linkDes:DebugElement[];

fdescribe('Testing Dashboard Component', () => {
  const routes:Routes = [
    {
      path:'user-profile', component:MockUserprofileComponent
    },
    {
      path:'gallery', component:MockUsergalleryComponent
    }
  ]
  let UsersServiceStub: Partial<UsersService>;
  beforeEach(async(() => {
    UsersServiceStub = {
      //isLoggedIn: true,
      //user: { name: 'Test User'}
    };
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        RouterLinkDirectiveStub
      ],
      providers:[{provide: UsersService, useValue: UsersServiceStub}],
      imports:[MockDashboardModule,MaterialComponentModule,RouterTestingModule.withRoutes(routes),BrowserAnimationsModule]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(DashboardComponent);
      component    = fixture.componentInstance;
      usersService = TestBed.get(UsersService);

    });
  }));
  tests();
});

function tests() {
  //let routerLinks: RouterLinkDirectiveStub[];
  //let linkDes: DebugElement[];

  beforeEach(() => {
    fixture.detectChanges(); // trigger initial data binding

    // find DebugElements with an attached RouterLinkStubDirective
    linkDes = fixture.debugElement
      .queryAll(By.directive(RouterLinkDirectiveStub));

    // get attached link directive instances
    // using each DebugElement's injector
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));

    console.log(routerLinks)

  });

  it('can instantiate the component', () => {
    expect(component).not.toBeNull();
  });

  it('can get RouterLinks from template', () => {
    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].linkParams).toBe('user-profile');
    expect(routerLinks[1].linkParams).toBe('gallery');
    //expect(routerLinks[2].linkParams).toBe('forgetpd-username');
  });

  it('click on the login page navigateTo login',fakeAsync(()=>{
    createComponent();
    //router.navigate(['login']);
    click(page.userProfileLinkDe)
    advance()
    expectPathToBe('/user-profile');
  }))
}

//added the page class
function createComponent() {
  const injector = fixture.debugElement.injector;
  location = injector.get(Location);// as SpyLocation;
  router = injector.get(Router);
  router.initialNavigation();
  // spyOn(injector.get(TwainService), 'getQuote')
  // // fake fast async observable
  //   .and.returnValue(asyncData('Test Quote'));
  // advance();

  page = new Page();
}

/**
 * Advance to the routed page
 * Wait a tick, then detect changes, and tick again
 */
function advance(): void {
  tick(); // wait while navigating
  fixture.detectChanges(); // update view
  tick(); // wait for async data to arrive
}
class Page {
    userProfileLinkDe :DebugElement
    galleryLinkDe: DebugElement
    //registerLinkDe : DebugElement

  // for debugging
  comp: DashboardComponent;
  location: SpyLocation;
  router: Router;
  fixture: ComponentFixture<DashboardComponent>;

  constructor() {
    const links = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    //this.forgetLinkDe     = links[2];
    this.userProfileLinkDe = links[0];
    this.galleryLinkDe    = links[1];

    // for debugging
    this.comp    = component;
    this.fixture = fixture;
    this.router  = router;
  }
}

function expectPathToBe(path: string, expectationFailOutput?: any) {
  expect(location.path()).toEqual(path, expectationFailOutput || 'location.path()');
}

function expectElementOf(type: Type<any>): any {
  const el = fixture.debugElement.query(By.directive(type));
  expect(el).toBeTruthy('expected an element for ' + type.name);
  return el;
}

function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
  if (el instanceof HTMLElement) {
    el.click();
  } else {
    el.triggerEventHandler('click', eventObj);
  }
}

const ButtonClickEvents = {
  left:  { button: 0 },
  right: { button: 2 }
};



