import { Component, OnInit } from '@angular/core';
import {UsersService} from "../services/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
  constructor(private userService:UsersService, private route: ActivatedRoute, private breakpointObserver: BreakpointObserver) {
    //constructor(){
  }

  ngOnInit() {

  }

}
