import { async, ComponentFixture, TestBed, } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { UsergalleryComponent } from './usergallery.component';
import { By } from '@angular/platform-browser';

fdescribe('UsergalleryComponent', () => {
  let component: UsergalleryComponent;
  let fixture: ComponentFixture<UsergalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsergalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsergalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).not.toBeNull();
  });

  it('should find the <p> with fixture.debugElement.query(By.css)', () => {
    //const bannerDe: DebugElement = fixture.debugElement;
    const paragraphDe = fixture.debugElement.query(By.css('.pp'));
    let p = paragraphDe.nativeElement;
    expect(p.textContent).toEqual('usergallery works!');
  });

});
