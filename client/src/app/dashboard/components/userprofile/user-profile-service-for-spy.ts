import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {UsersDataVo} from "../../../datavo/users-data-vo";
import {UserDataVo} from "../../../datavo/user-data-vo";

import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({providedIn:'root'})
export class usersServiceMock {
 baseUrl:string = 'http://localhost:3000';
 usersUrl:string = this.baseUrl+'/users';

  constructor(private http: HttpClient) { }

  /** GET heroes from the server */
  getUsers():Observable<UsersDataVo>{
    return this.http.get(this.usersUrl) as Observable<UsersDataVo>
  }
}