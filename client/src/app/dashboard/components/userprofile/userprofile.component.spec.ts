import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import { By } from '@angular/platform-browser';
import { UserprofileComponent } from './userprofile.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialComponentModule } from '../../../app.module';
import { UsersService } from '../../../services/users.service';
import { DebugElement,EventEmitter } from '@angular/core';
import { StorageServiceModule } from 'angular-webstorage-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterTestingModule} from "@angular/router/testing";
import { Observable } from 'rxjs';


export class MockUserDataVo {
constructor(public _id: string,
public name: string,
public username: string,
public email: string,
public password: string,
public mobile: string,
public address: string,
public pincode: string) { }
}

 

fdescribe('UserprofileComponent', () => {
  let component: UserprofileComponent;
  let fixture: ComponentFixture<UserprofileComponent>;
  let userService:UsersService;
  let uInfo= {
    _id: '200',
    name:'jay',
    username:'jkushwaha',
    email:'jay@gmail.com',
    password:'jayy123',
    mobile:'',
    address:'',
    pincode:''
  }
   class MockUserDataVo {
    _id: string = '200';
    name: string = 'jay';
    username: string = 'jkushwaha';
    email: string = 'jay@gmail.com';
    password: string = 'jayy123';
    mobile: string = '';
    address: string = '';
    pincode: string = ''
    constructor(){ }
  } 
  
  
  class MockUsersDataVo {
    status:string = '200';
    success:boolean = true;
    userinfo:MockUserDataVo = new MockUserDataVo();
    token:string = 'kjlsjfjfdljf';
    constructor(){ }
  }
  
   class MockUserService extends UsersService{
    //constructor(){}
    usersData = new MockUsersDataVo();
    userData = new MockUserDataVo();
    isLogin= true;
   } 

  /*let userServiceStub: Partial<UsersService>;
  userServiceStub = {
    isLogin: true
  };*/

  beforeEach(() => {
    // stub UserService for test purposes
    //const userServiceSpy = spyOn( MockUserService, 'usersData')
    TestBed.configureTestingModule({
      declarations: [ UserprofileComponent ],
      imports: [ReactiveFormsModule, FormsModule, MaterialComponentModule,StorageServiceModule,RouterTestingModule,BrowserAnimationsModule,HttpClientTestingModule],
      providers: [{provide: UsersService, useClass: MockUserService },HttpClient],
      //providers: [UsersService, HttpClient ]
    })
  
    fixture = TestBed.createComponent(UserprofileComponent);
    component    = fixture.componentInstance;
    component.ngOnInit();
    // UserService from the root injector
    userService = TestBed.get(UsersService);
    //this.userData=userService.userData;

  });



  

 
  
  it('should request login if not logged in', () => {
    //fixture.detectChanges();
    console.log('isLogin='+userService.isLogin);
    userService.isLogin=true;
    expect(userService.isLogin).toMatch('true');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    component.isEdit=false;
    component.userData=userService.userData;
    console.log('raj='+component.userData._id);
    //component.ngOnInit();
    fixture.detectChanges();
    const userDe = fixture.debugElement.query(By.css('#user-details'));
    console.log('allElColl='+userDe);
    const allElColl=userDe.nativeElement.getElementsByTagName('p');
    console.log('allElColl='+allElColl);
    

  });

  it('should find the <save> with fixture.debugElement.query(By.css)', () => {
    //const bannerDe: DebugElement = fixture.debugElement;
    const saveDe = fixture.debugElement.query(By.css('.save'));
    let save = saveDe.nativeElement;
    fixture.detectChanges();
    console.log(save.textContent);
    expect(save.textContent).toEqual('save User Details');
  });
 

  /*it('submitting a form emits a user', () => {
    expect(component.editForm.valid).toBeFalsy();
    component.editForm.controls['user_id'].setValue("200");
    component.editForm.controls['name'].setValue("jay");
    component.editForm.controls['username'].setValue("jkushwaha");
    component.editForm.controls['password'].setValue("jayy123");
    component.editForm.controls['email'].setValue("jay@gmail.com");
    component.editForm.controls['mobile'].setValue("123456789");
    component.editForm.controls['address'].setValue("A123456789");
    component.editForm.controls['pincode'].setValue("123456789");
    expect(component.editForm.valid).toBeTruthy();

    let userDetail:MockUserDataVo;
    const saveDe = fixture.debugElement.query(By.css('.save'));
   // let save = saveDe.nativeElement;
    userService.updateUser(component.editForm.value,"200").subscribe(function(value){
      userDetail = value
      console.log('userdetail'+userDetail);
    } );
    
    saveDe.triggerEventHandler('click', null);*/


/*

    let updated = new EventEmitter<MockUserDataVo>();

    let userData:MockUserDataVo;
    if (component.editForm.valid) {
      updated.emit(
          new MockUserDataVo(
            component.editForm.value._id,
            component.editForm.value.name,
            component.editForm.value.username,
            component.editForm.value.password,
            component.editForm.value.email,
            component.editForm.value.mobile,
            component.editForm.value.address,
            component.editForm.value.pinecode,
          )
      );
    }*/
    // Subscribe to the Observable and store the user in a local variable.
    //..updated.subscribe((value) => userData = value);
    //console.log(userData.name);
    // Trigger the update function
    // Now we can check to make sure the emitted value is correct
    //console.log(userDetail.name+"::"+userDetail.username);
    //expect(userDetail.name).toBe("jay");
    //expect(userDetail.username).toBe("jkushwaha");
  //});

  it('should find the <edit> with fixture.debugElement.query(By.css)', () => {
    const editDe=fixture.debugElement.query(By.css('.edit'));
    let edit = editDe.nativeElement;
    fixture.detectChanges();
    console.log(edit.textContent)
    expect(edit.textContent).toEqual('Edit User Details');
  });



it('should click edit profile button', fakeAsync( () => {
  //fixture.detectChanges();
  spyOn(component, 'editUser'); //method attached to the click.
  let btn = fixture.debugElement.query(By.css('.edit'));
  btn.triggerEventHandler('click', null);
  tick(); // simulates the passage of time until all pending asynchronous activities finish
  
  fixture.detectChanges();
  expect(component.editUser).toHaveBeenCalled();
}));

it('should click save profile button', fakeAsync( () => {
  //fixture.detectChanges();
  spyOn(component, 'saveUser'); //method attached to the click.
  let btn = fixture.debugElement.query(By.css('.save'));
  btn.triggerEventHandler('click', null);
  tick(); // simulates the passage of time until all pending asynchronous activities finish
  
  fixture.detectChanges();
  expect(component.saveUser).toHaveBeenCalled();
}));



});
