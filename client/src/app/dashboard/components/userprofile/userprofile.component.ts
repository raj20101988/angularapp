import { Component, OnInit, Inject } from '@angular/core';
import {UsersService} from "../../../services/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserDataVo} from "../../../datavo/user-data-vo";
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
//import {jwtDecode} from 'jwt-decode';
import * as jwt_decode from "jwt-decode";
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';

declare var jwtDecode:any

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
   errorMessage:boolean;
   userData:UserDataVo;
   isEdit:boolean=false;
   editForm:FormGroup;
   emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    mobilepattern = /[0-9\+\-\ ]/;

  constructor(private formBuilder:FormBuilder, private userService:UsersService) { }

  ngOnInit() {
      this.userData=this.userService.userData;
      console.log(this.userData);
      //for taking jwt token from localstorage and decoding , use the below line
      //let jwtToken:any = this.getDecodedAccessToken(this.localStorage.get('token'));
     // console.log(jwtToken._id);
   
    this.editForm = this.formBuilder.group({
      user_id: [this.userData._id, Validators.required],
      name: [this.userData.name, Validators.required],
      username: [this.userData.username, Validators.required],
      password: [this.userData.password, [Validators.required, Validators.minLength(6)]],
      email: [this.userData.email, [Validators.required,Validators.pattern(this.emailPattern)]],
      mobile: [this.userData.mobile, [Validators.required,Validators.pattern(this.mobilepattern)]],
      address: [this.userData.address, Validators.required],
      pincode: [this.userData.pincode, Validators.required] 
     /*  user_id: [' ', Validators.required],
      name: [' ', Validators.required],
      username: [' ', Validators.required],
      password: [' ', [Validators.required, Validators.minLength(6)]],
      email: [' ', [Validators.required,Validators.pattern(this.emailPattern)]],
      mobile: [' ', [Validators.required,Validators.pattern(this.mobilepattern)]],
      address: [' ', Validators.required],
      pincode: [' ', Validators.required]*/
    });
  }

  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }

  get f() { return this.editForm.controls; }

  editUser(){
    this.isEdit=true;
  }

  saveUser(){
    
    console.log(this.userData._id+':::saved'+this.userData.name);
    this.userService.updateUser(this.editForm.value,this.userData._id).subscribe(data =>{
      this.userService.userData=data as UserDataVo;
      this.userData=data as UserDataVo;
      console.log(data);
      //this.editForm.setValue(data);
      this.isEdit=false;
      this.userService.isLogin = true;
    }, error =>{
      this.errorMessage = error.status === 401
      console.log(this.errorMessage+'User is not created'+error.status)
    })
  }
}
