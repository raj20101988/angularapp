import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule,ReactiveFormsModule} from "@angular/forms";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {DashboardComponent} from "./dashboard.component";
import { UserprofileComponent } from "./components/userprofile/userprofile.component";
import { MaterialComponentModule } from '../app.module';
import { UsergalleryComponent } from './components/usergallery/usergallery.component';
//import { NotFoundComponent } from "./../authentication/components/not-found/not-found.component";

@NgModule({
  imports: [
    CommonModule, DashboardRoutingModule,MaterialComponentModule,ReactiveFormsModule
  ],
  declarations: [DashboardComponent,UserprofileComponent, UsergalleryComponent]
})
export class DashboardModule { }
