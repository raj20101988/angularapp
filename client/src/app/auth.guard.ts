import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {UsersService} from "./services/users.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router:Router, private userService:UsersService){

  }

  canActivate(): boolean {
    if(this.userService.isLogin){
      console.log("user is authenticated");
      return true;
    }else{
      this.router.navigate(['authentication']);
      return false;
    }
  }
}
