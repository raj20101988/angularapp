import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthenticationRoutingModule} from "./authentication-routing.module";
import {AuthenticationComponent} from "./authentication.component";
import {LoginComponent} from "./components/login/login.component";
import {ForgetpasswordorusernameComponent} from "./components/forgetpasswordorusername/forgetpasswordorusername.component";
import {NotFoundComponent} from "./components/not-found/not-found.component";
import {UsersComponent} from "./components/register/users.component";
import {MaterialComponentModule} from "../app.module";

@NgModule({
  imports: [
    CommonModule,AuthenticationRoutingModule,MaterialComponentModule
  ],
  declarations: [AuthenticationComponent,LoginComponent,ForgetpasswordorusernameComponent,NotFoundComponent,UsersComponent]
})
export class AuthenticationModule { }
