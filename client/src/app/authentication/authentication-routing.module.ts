import { NgModule, Component } from '@angular/core';
import {RouterModule, Route, Routes} from "@angular/router";
import { AuthenticationComponent } from './authentication.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/register/users.component';
import { ForgetpasswordorusernameComponent } from './components/forgetpasswordorusername/forgetpasswordorusername.component';
//import { NotFoundComponent } from './components/not-found/not-found.component';

const authenticationRoutes: Routes = [
{
  path: '',
  component : AuthenticationComponent,
    children: [
      { path: 'login', component:LoginComponent},
      { path: 'register', component:UsersComponent},
      { path: 'forgetpd-username', component:ForgetpasswordorusernameComponent},
      { path: 'user-profile', component:ForgetpasswordorusernameComponent},
      //{ path: '',   redirectTo: 'login', pathMatch: 'full' },
      //{ path: '**', component:NotFoundComponent }
    ]
}
];


/*{
  path: '',
  component: LayoutComponent,
  children: [
      { path: '', redirectTo: 'dashboard' },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
      { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
      { path: 'forms', loadChildren: './form/form.module#FormModule' },
      { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
      { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
      { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
      { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
  ]
}*/



@NgModule({
  imports: [
    RouterModule.forChild(authenticationRoutes)
  ],
  exports:[RouterModule]
})
export class AuthenticationRoutingModule { }
