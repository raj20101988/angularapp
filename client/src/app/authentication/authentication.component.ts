import { Component, OnInit } from '@angular/core';
import {Router, RouterLinkActive} from "@angular/router";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    console.log(":::::"+ this.router.isActive(this.router.routerState.snapshot.url,true));
    //this.router.navigate(['login']);
  }

}
