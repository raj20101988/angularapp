import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgetpasswordorusernameComponent } from './forgetpasswordorusername.component';

describe('ForgetpasswordorusernameComponent', () => {
  let component: ForgetpasswordorusernameComponent;
  let fixture: ComponentFixture<ForgetpasswordorusernameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgetpasswordorusernameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgetpasswordorusernameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
