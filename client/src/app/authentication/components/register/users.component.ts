import { Component, OnInit, OnChanges } from '@angular/core';
import {UsersService} from "../../../services/users.service";
import {FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {Router} from "@angular/router";
import {UserDataVo} from "../../../datavo/user-data-vo";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit,OnChanges {

  registerForm:FormGroup;
  user:string;
  private userData:UserDataVo;
  //unamePattern = "^[a-z0-9_-]{8,15}$";
  pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,16}$";//combination of small and capital letter and min 6 character max 12.
  //mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private formBuilder:FormBuilder,private userService:UsersService,private router:Router, private auth:AuthService) {

   }

  ngOnChanges(){
    //Always called when input bound value will be changed...
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required,Validators.pattern(this.emailPattern)]]
    })

    //Called only once in component life cycle hooks
   /*  this.userService.broadCastEvent.subscribe(user => this.user = user);

    this.userService.getUsers().subscribe((data)=>{
      //console.log(this.userData.username+"::"+this.userData.password);
    }) */
  }

  get f() { return this.registerForm.controls; }

  doRegister(){
    //console.log('do register'+this.userData.username,this.userData.password,this.userData.email);
    console.log(':::::='+this.registerForm.value);
    this.userService.createUsers(this.registerForm.value).subscribe((data) =>{
      console.log(data);
      this.auth.clearStorage();
      //this.auth.isAuthenticated();
      this.router.navigate(['authentication/login']);
    })
  }

}
