import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {MaterialComponentModule} from "../../../app.module";
import { UsersComponent } from './users.component';
import {UsersService} from "../../../services/users.service";
import {AuthService} from "../../../services/auth.service";
import {RouterTestingModule} from "@angular/router/testing";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

fdescribe('Users Register Component', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let userServiceStub = {
    isLogin: true
  }
   class AuthServiceStub extends AuthService {
    getToken(){ return "123456"}
  }
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MaterialComponentModule,RouterTestingModule,BrowserAnimationsModule],
      declarations: [ UsersComponent ],
      providers: [{provide: UsersService, useValue: userServiceStub},{provide:AuthService, useValue:AuthServiceStub}]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    console.log('component.registerForm.invalid='+component.registerForm.invalid);
    expect(component.registerForm.invalid).toBeTruthy();
  });
  it('email field validity', () => {
    let errors = {};
    let email = component.registerForm.controls['email'];
    expect(email.valid).toBeFalsy();

    // Email field is required
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();

    // Set email to something
    email.setValue("test");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    // Set email to something correct
    email.setValue("test@example.com");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });


  it('password field validity', () => {
    let errors = {};
    let password = component.registerForm.controls['password'];
    console.log(password.invalid)
    errors = password.errors || {};
    // expect(errors['minLen']).toBeTruthy();
    password.setValue('123456');
    expect(password.valid).toBeTruthy();
    expect(password.value).toBe('123456');
  });

  it('username field validity', () => {
    let errors = {};
    let username = component.registerForm.controls['username'];
    console.log(username.invalid+"::"+username.errors)
    username.setValue('jay');
    expect(username.valid).toBeTruthy();
    expect(username.value).toBe('jay');
  });
});

