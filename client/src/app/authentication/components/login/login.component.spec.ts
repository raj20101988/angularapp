import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {UsersService} from "../../../services/users.service";
import {DebugElement} from "@angular/core";
import {UserDataVo} from "../../../datavo/user-data-vo";
import {AbstractControl, FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialComponentModule} from "../../../app.module";
import {RouterTestingModule} from "@angular/router/testing";
import {StorageServiceModule} from "angular-webstorage-service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userServiceStub:Partial<UsersService>;
  let userService:UsersService;
  let el:DebugElement;
  let username:AbstractControl;
  let password:AbstractControl;
  userServiceStub = {
    isLogin: true
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialComponentModule, StorageServiceModule, RouterTestingModule,BrowserAnimationsModule],
      declarations: [ LoginComponent ],
      providers: [{provide: UsersService, useValue: userServiceStub }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UsersService);
    //username = component.loginForm.controls.username
    fixture.detectChanges();
    component.ngOnInit();
  }));
  it('form invalid when empty', () => {
    expect(component.loginForm.invalid).toBeFalsy();
  });

  it('password field validity', () => {
    let errors = {};
    let password = component.loginForm.controls['password'];
    console.log(password.invalid)
    errors = password.errors || {};
    // expect(errors['minLen']).toBeTruthy();
    password.setValue('123456');
    expect(password.valid).toBeTruthy();
    expect(password.value).toBe('123456');
  });

  it('username field validity', () => {
    let errors = {};
    let username = component.loginForm.controls['username'];
    console.log(username.invalid+"::"+username.errors)
    username.setValue('jay');
    expect(username.valid).toBeTruthy();
    expect(username.value).toBe('jay');
  });

  it('should request login if not logged in', () => {
    fixture.detectChanges();
    expect(userService.isLogin).toEqual(true);
  });
});
