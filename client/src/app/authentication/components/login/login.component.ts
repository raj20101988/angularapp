import { Component, OnInit, Inject } from '@angular/core';
import {UsersService} from "../../../services/users.service";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {UserDataVo} from "../../../datavo/user-data-vo";
import {UsersDataVo} from "../../../datavo/users-data-vo";
//import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
//import {JwtHelperService} from '@auth0/angular-jwt'

declare var $:any

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //Forms controls

  //^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})
  //rgx:RegExp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  //we can handle error message in component class also like this.
  // getErrorMessage() {
  //   return this.password.hasError('required') ? 'You must enter a value' :
  //     this.password.hasError('pattern') ? 'Not a valid email' : this.password.hasError('maxLength') ? 'Password must be less than 16 characters':
  //       '';
  // }
  // This regex will enforce these rules:
  //
  //   At least one upper case English letter, (?=.*?[A-Z])
  // At least one lower case English letter, (?=.*?[a-z])
  // At least one digit, (?=.*?[0-9])
  // At least one special character, (?=.*?[#?!@$%^&*-])
  // Minimum eight in length .{8,} (with the anchors)

  loginForm:FormGroup;
  user:string;
  editUser:string;
  isLogin: Boolean = true;
  errorMessage:boolean = false;
  private userData:UserDataVo
  constructor(private fb:FormBuilder,public userService:UsersService,private router:Router) { }

  ngOnInit() {
    this.userData = this.userService.userData;
    //we can create nested form group.
    this.loginForm = this.fb.group({
      username: '',
      password: ['',[Validators.minLength(6)]]
    })
    //this.userService.broadCastEvent.subscribe(user => this.user = user);
  }

  get f() { return this.loginForm.controls; }

  editTheUser(){
    this.userService.getNewUser(this.editUser);
  }
  doLogin(){
      console.info(this.f.username.value);
      if(this.loginForm.invalid){
        return;
      }
      this.userData.username = this.f.username.value;
      this.userData.password = this.f.password.value;

      this.userService.doLogin(this.userData).subscribe(data =>{
      console.log(data.token);
      localStorage.setItem('token',data.token);
      this.userService.userData = data.userinfo as UserDataVo
      this.userService.isLogin = true;
      this.isLogin = false;
      this.errorMessage = false;
      //usage of JwtHelperService to decode the token
      // const jwtHelper = new JwtHelperService();
      // let token = jwtHelper.decodeToken(this.localStorage.get('token'));
      // console.log(token._id)
      this.router.navigate(['dashboard']);
    }, error =>{
      this.errorMessage = error.status === 401
      console.log(this.errorMessage+'User is not created'+error.status)
    });

  }
  doRegister(){
    alert("clicked register going to backup folder");
  }

}
