import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { AuthenticationComponent } from './authentication.component';
import {RouterTestingModule} from "@angular/router/testing";
import {Router, RouterLinkWithHref, Routes} from "@angular/router";
import {LoginComponent} from "./components/login/login.component";
import {Component, DebugElement, NgModule, Type} from "@angular/core";
import {ForgetpasswordorusernameComponent} from "./components/forgetpasswordorusername/forgetpasswordorusername.component";
import {SpyLocation} from "@angular/common/testing";
import {By} from "@angular/platform-browser";
import {RouterLinkDirectiveStub, RouterStubsModule} from "../../testing/router-link-directive-stub";
import {Location} from "@angular/common";
@Component({ template: '' })
class MockLoginComponent {}
@Component({ template: '' })
class MockUsersComponent {}

@Component({ template: '' })
class MockForgetpasswordorusernameComponent { }


@NgModule({
  declarations: [MockLoginComponent,MockUsersComponent,MockForgetpasswordorusernameComponent],
  exports:      [MockLoginComponent,MockUsersComponent,MockForgetpasswordorusernameComponent]
})
class MockAuthenticationModule { }

let component: AuthenticationComponent;
let fixture: ComponentFixture<AuthenticationComponent>;
let location:Location;
let router:Router
let page:Page
let routerLinks:RouterLinkDirectiveStub[];
let linkDes:DebugElement[];

fdescribe('Testing Authentication component', () => {
  const routes:Routes = [
    {
      path:'login', component:MockLoginComponent
    },
    {
      path:'register', component:MockUsersComponent
    },
    { path: 'forgetpd-username', component:MockForgetpasswordorusernameComponent}
  ]
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthenticationComponent,
        RouterLinkDirectiveStub
      ],
      imports:[MockAuthenticationModule,RouterTestingModule.withRoutes(routes)]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(AuthenticationComponent);
      component    = fixture.componentInstance;
    });
  }));
  tests();
});

function tests() {
  //let routerLinks: RouterLinkDirectiveStub[];
  //let linkDes: DebugElement[];

  beforeEach(() => {
    fixture.detectChanges(); // trigger initial data binding

    // find DebugElements with an attached RouterLinkStubDirective
    linkDes = fixture.debugElement
      .queryAll(By.directive(RouterLinkDirectiveStub));

    // get attached link directive instances
    // using each DebugElement's injector
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));

    console.log(routerLinks)

  });

  it('can instantiate the component', () => {
    expect(component).not.toBeNull();
  });

  it('can get RouterLinks from template', () => {
    expect(routerLinks.length).toBe(3, 'should have 3 routerLinks');
    expect(routerLinks[0].linkParams).toBe('login');
    expect(routerLinks[1].linkParams).toBe('register');
    expect(routerLinks[2].linkParams).toBe('forgetpd-username');
  });

  it('click on the login page navigateTo login',fakeAsync(()=>{
    createComponent();
    //router.navigate(['login']);
    click(page.loginLinkDe)
    advance()
    expectPathToBe('/login');
  }))
}

//added the page class
function createComponent() {
  const injector = fixture.debugElement.injector;
  location = injector.get(Location);// as SpyLocation;
  router = injector.get(Router);
  router.initialNavigation();
  // spyOn(injector.get(TwainService), 'getQuote')
  // // fake fast async observable
  //   .and.returnValue(asyncData('Test Quote'));
  // advance();

  page = new Page();
}

/**
 * Advance to the routed page
 * Wait a tick, then detect changes, and tick again
 */
function advance(): void {
  tick(); // wait while navigating
  fixture.detectChanges(); // update view
  tick(); // wait for async data to arrive
}
class Page {
    forgetLinkDe :DebugElement
    loginLinkDe: DebugElement
    registerLinkDe : DebugElement

  // for debugging
  comp: AuthenticationComponent;
  location: SpyLocation;
  router: Router;
  fixture: ComponentFixture<AuthenticationComponent>;

  constructor() {
    const links = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));
    this.forgetLinkDe     = links[2];
    this.loginLinkDe = links[0];
    this.registerLinkDe    = links[1];

    // for debugging
    this.comp    = component;
    this.fixture = fixture;
    this.router  = router;
  }
}

function expectPathToBe(path: string, expectationFailOutput?: any) {
  expect(location.path()).toEqual(path, expectationFailOutput || 'location.path()');
}

function expectElementOf(type: Type<any>): any {
  const el = fixture.debugElement.query(By.directive(type));
  expect(el).toBeTruthy('expected an element for ' + type.name);
  return el;
}

function click(el: DebugElement | HTMLElement, eventObj: any = ButtonClickEvents.left): void {
  if (el instanceof HTMLElement) {
    el.click();
  } else {
    el.triggerEventHandler('click', eventObj);
  }
}

const ButtonClickEvents = {
  left:  { button: 0 },
  right: { button: 2 }
};


