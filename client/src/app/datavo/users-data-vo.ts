import {UserDataVo} from "./user-data-vo";

export class UsersDataVo {
  status:string
  success:boolean
  userinfo:UserDataVo
  token:string
}
