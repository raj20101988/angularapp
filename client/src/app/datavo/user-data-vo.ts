export class UserDataVo {
  _id: string;
  name: string;
  username: string;
  email: string;
  password: string;
  mobile: string;
  address: string;
  pincode: string;
}
