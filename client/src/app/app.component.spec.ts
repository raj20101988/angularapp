import {TestBed, async, fakeAsync, tick} from '@angular/core/testing';
import { AppComponent } from './app.component';
import {RouterTestingModule} from "@angular/router/testing";
import {Router, Routes} from "@angular/router";
import {Component, NgModule} from "@angular/core";
import {Mock} from "protractor/built/driverProviders";
import {Location} from "@angular/common";


@Component({ template: '' })
class MockDashboardComponent {}
@Component({ template: '' })
class MockAuthenticationComponent {}
@NgModule({
  declarations: [MockAuthenticationComponent,MockDashboardComponent],
  exports:      [MockAuthenticationComponent,MockDashboardComponent]
})
class MockModule { }

fdescribe('AppComponent', () => {
  let location: Location;
  let router: Router;
  let fixture;
  let app;
  const routes:Routes = [
    {
      path:'authentication',
      component:MockAuthenticationComponent
    },
    {
      path:'dashboard',
      component:MockDashboardComponent
    }
  ]
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports:[MockModule,RouterTestingModule.withRoutes(routes)]
    }).compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(AppComponent);
    app=fixture.debugElement.componentInstance;
    router.initialNavigation();
    fixture.detectChanges();//it call initial data binding in component otherwise you need to call in 'it' section.
  }));


  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'App Creator'`, async(() => {
    expect(app.title).toEqual('App Creator');
  }));

  it('should render title in a h1 tag', async(() => {
    //fixture.detectChanges();// I have commented this section because i have already called above so intial app.title value
    //is detected. but its good practice to call here also.
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to App Creator!');
  }));

  it('navigate to "" redirects you to /authentication', fakeAsync(() => {
    router.navigate(['authentication']);
    tick();
    expect(location.path()).toBe('/authentication');
  }));
  it('navigate to "" redirects you to /dashboard', fakeAsync(() => {
    router.navigate(['dashboard']);
    tick();
    expect(location.path()).toBe('/dashboard');
  }));

});
