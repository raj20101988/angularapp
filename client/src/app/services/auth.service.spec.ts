import { TestBed, inject,async } from '@angular/core/testing';
import {LOCAL_STORAGE, WebStorageService, StorageService} from "angular-webstorage-service";
import { AuthService } from './auth.service';
import { InjectionToken } from '@angular/core';

/*export class MockAuthService{
  constructor(){}
  getToken(){
    return 'gggdgdgdgdg';
  }
    
}*/
fdescribe('AuthService', () => {
  let webStorage = {
    LOCAL_STORAGE:WebStorageService
  }

  
  let service:AuthService
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService,{provide:LOCAL_STORAGE, useValue:webStorage.LOCAL_STORAGE}]
    });

    service = TestBed.get(AuthService)
  });

   it('should be created', inject([AuthService], (service: AuthService) => {
     expect(service).toBeTruthy();
   }));

   it('should be created testing local', async(() => {
    // spyOn(service,'getToken').and.returnValue('abcd');
     localStorage.setItem('token','abcd');
    expect(service.getToken()).not.toBe('');
  }));

   

});
