import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

import {Observable} from 'rxjs/Observable'
import {UserDataVo} from "../datavo/user-data-vo";
import {UsersDataVo} from "../datavo/users-data-vo";
//import {UsersDataLoginVo} from "../datavo/users-data-login-vo";
import { tap, catchError } from 'rxjs/operators';
//import { CreateUserDataVo } from '../datavo/create-user-data-vo';

@Injectable()
export class UsersService {
  private baseUrl:string = 'http://localhost:3000'

  private user:BehaviorSubject<string> = new BehaviorSubject <string>('jaychandra kushwaha');
  broadCastEvent = this.user.asObservable();


  userData:UserDataVo = new UserDataVo();
  isLogin: Boolean = false;
  isRegister :Boolean = false;
  isNeedHelp: Boolean  = false;
  isForgetUserOrPd:Boolean = false;
  usersUrl:string = this.baseUrl+'/users';
  constructor(private http:HttpClient) { }

  getNewUser(newUser){
    console.log(newUser)
    this.user.next(newUser);
  }

  createUsers(obj:UserDataVo){
    console.log(obj)
    return this.http.post(this.usersUrl,obj) as Observable<UsersDataVo>;
  }

  getUsers():Observable<UsersDataVo>{
    return this.http.get(this.usersUrl) as Observable<UsersDataVo>
  }

  doLogin(obj:UserDataVo){
    console.log(JSON.stringify(obj))
    let loginUrl:string = this.baseUrl + '/login';
    return this.http.post(loginUrl,obj) as Observable<UsersDataVo> ;
  }

  /** PUT: update the User on the server */
  updateUser(obj:UserDataVo,userId:string): Observable<any> {
    return this.http.put(this.usersUrl+'/'+userId, obj) as Observable<UserDataVo>;
    //.pipe(
      //tap(_ => console.log(`updated hero id=${obj.user_id}`)),
      //catchError(this.handleError<any>('updateHero'))
    //);
  }

}
