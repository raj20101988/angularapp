import { TestBed, inject, async, fakeAsync } from '@angular/core/testing';

import { UsersService } from './users.service';
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
//import { UserDataVo } from '../datavo/user-data-vo';
//import {UsersDataLoginVo} from '../datavo/users-data-login-vo';
import { UserDataVo } from '../datavo/user-data-vo';
import { UsersDataVo } from '../datavo/users-data-vo';
//import { CreateUserDataVo } from '../datavo/create-user-data-vo';

class MockUserDataVo {
  _id: string = '200';
  name: string = 'jay';
  username: string = 'jkushwaha';
  email: string = 'jay@gmail.com';
  password: string = 'jayy123';
  mobile: string = '';
  address: string = '';
  pincode: string = ''
  constructor(){ }
}
class MockUserLoginData{
  username: string = 'jkushwaha';
  password: string = 'jayy123';
  constructor(){}
}



class MockUsersDataVo {
  status:string = '200';
  success:boolean = true;
  userinfo:MockUserDataVo = new MockUserDataVo();
  token:string = 'kjlsjfjfdljf';
  constructor(){ }
}

class MockUserService extends UsersService{
  usersData = new MockUsersDataVo();
  userData = new MockUserDataVo();
  mockUserLoginData=new MockUserLoginData();
 }



/*fdescribe('UsersService', () => {
  //let service:UsersService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [UsersService,HttpClient]
    });
  });

  it('should be created', inject([UsersService], (service: UsersService) => {
    expect(service).toBeTruthy();
    console.log(service.userData);

  }));
});*/

fdescribe('HttpClient testing', () => {
  let httpClient: HttpClient;
  let httpTestingController:HttpTestingController;
  let usersService:MockUserService;
  

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers:[MockUserService,HttpClient]  
    });

    // Inject the http service and test controller for each test
    usersService = TestBed.get(MockUserService);
    httpTestingController = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created using inject method', inject([MockUserService], (service: MockUserService) => {
    expect(service).toBeTruthy();
    console.log(service.userData);

  }));

  it('should be created', () => {
    expect(usersService).toBeTruthy();
    console.log(usersService.userData);
  });

  /// Tests begin  login users///
  it('should login user using HttpClient.post', fakeAsync(() => {
   // Make an HTTP GET request
    usersService.doLogin(usersService.userData)
      .subscribe(data =>{
        // When observable resolves, result should match test data
        console.log(data.status);
        console.log(usersService.userData);
        expect(data.status).toEqual(usersService.usersData.status);
        expect(data.userinfo).toEqual(usersService.userData);
      });

    // The following `expectOne()` will match the request's URL.
    // If no requests or multiple requests matched that URL
    // `expectOne()` would throw.
    const req = httpTestingController.expectOne('http://localhost:3000/login');

    // Assert that the request is a POST.
    //expect(req.request.method).toEqual('GET');//it will fail

    expect(req.request.method).toEqual('POST');//it will pass.

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
   req.flush(usersService.usersData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  }));
  
  /// Tests begin for update users///
  it('should update user detail using HttpClient.put', () => {
    let mockId="200";
    /*let mockUserinfo:UserDataVo = {_id: "1234",
      name: "kkr",
      username: "yui",
      email: "rr@www.com",
      password: "hhh",
      mobile: "6789054321",
      address: "678",
      pincode: "90"};*/

    // Make an HTTP GET request
    usersService.updateUser(usersService.userData,mockId)
      .subscribe(data =>{
        // When observable resolves, result should match test data
       // console.log(data);
        //console.log(mockUserinfo);
        //expect(data).toEqual(mockUserinfo);

        console.log(data.status);
        console.log(usersService.userData);
        expect(data.status).toEqual(usersService.usersData.status);
        expect(data.userinfo).toEqual(usersService.userData);


      });

    // The following `expectOne()` will match the request's URL.
    // If no requests or multiple requests matched that URL
    // `expectOne()` would throw.
    const req = httpTestingController.expectOne('http://localhost:3000/users/200');

    // Assert that the request is a PUT.
    expect(req.request.method).toEqual('PUT');

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
   req.flush(usersService.usersData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  /// Tests begin for create users///
  it('should create users using HttpClient.post', () => {
    /*let mockCreateUserInfo:CreateUserDataVo = {
      username: "yui",
      email: "rr@www.com",
      password: "hhh"
    };

    let mockUserinfo:UserDataVo = {_id: "1234",
      name: "kkr",
      username: "yui",
      email: "rr@www.com",
      password: "hhh",
      mobile: "6789054321",
      address: "678",
      pincode: "90"};*/

    // Make an HTTP POST request
    usersService.createUsers(usersService.userData)
      .subscribe(data =>{
        // When observable resolves, result should match test data
        console.log(data);
        console.log(usersService.usersData);
        //expect(data.status).toEqual(usersService.usersData.status);
        expect(data).toEqual(usersService.usersData);
      });

    // The following `expectOne()` will match the request's URL.
    // If no requests or multiple requests matched that URL
    // `expectOne()` would throw.
    const req = httpTestingController.expectOne('http://localhost:3000/users');

    // Assert that the request is a POST.
    expect(req.request.method).toEqual('POST');

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
   req.flush(usersService.usersData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });


});
