import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from "angular-webstorage-service";
//import decode from 'jwt-decode';
@Injectable()
export class AuthService {
  jwttoken:string = '';
  success:boolean = false;
  constructor(){}
    getToken(): any {
    console.log("I am here finding the token...");
    return localStorage.getItem('token') ? localStorage.getItem('token') :'';
  }

  clearStorage(){
    localStorage.removeItem('token');
    console.log('clear token::'+localStorage.getItem('token'))
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    console.log(token);
    // return a boolean reflecting
    // whether or not the token is expired
    return true;//tokenNotExpired(null, token);
  }
}
