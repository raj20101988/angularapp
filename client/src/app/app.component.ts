import {} from "googlemaps";
import { Component, OnInit, AfterContentInit } from '@angular/core';
import { ViewChild } from '@angular/core';
//import { ViewChild } from '@angular/core';
//import { } from '@types/googlemaps';
declare var google: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterContentInit{

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  title = 'App Creator';
  name= 'rajnish';
  surname:'kumar';


  ngOnInit() {
    
    
  }

  ngAfterContentInit(){
    var mapProp = {
      center: new google.maps.LatLng(18.5793, 73.8143),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    console.log('after content init');
  }

  findDis(lat1,long1,lat2,long2){
    console.log(lat1,long1,lat2,long2);
    //let lat=28.7041;
    //let long=77.1025
    let delhi=new google.maps.LatLng(lat1, long1);

   // let lat1=19.0760;
    //let long1=72.8777;
    let mum=new google.maps.LatLng(lat2, long2);
    let dis=google.maps.geometry.spherical.computeDistanceBetween (delhi, mum);
    console.log(dis);
  }
  
}
