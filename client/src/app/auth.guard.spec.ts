import {TestBed, async} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from "@angular/common/http";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {CommonModule} from '@angular/common';
import 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
//import {LoggedInGuard} from './loggedin.guard';
import { AuthGuard } from './auth.guard';
import { UsersService } from './services/users.service';
import { state } from '@angular/animations';
//import {CookieService} from 'angular2-cookie/core';

fdescribe('Logged in guard should', () => {
    let authGuard: AuthGuard;
    let usersService:UsersService;
    let router = {
        navigate: jasmine.createSpy('navigate')
    };
// async beforeEach
beforeEach(async(() => {
  TestBed.configureTestingModule({
      imports: [FormsModule, CommonModule, HttpModule,HttpClientModule],
      providers: [AuthGuard, UsersService,
          {provide: Router, useValue: router}
      ]
  })
      .compileComponents(); // compile template and css
}));

// synchronous beforeEach
beforeEach(() => {
  authGuard = TestBed.get(AuthGuard);
  usersService = TestBed.get(UsersService);
});

it('be able to hit route when user is logged in', () => {
  usersService.isLogin = true;
  let next:ActivatedRouteSnapshot;
  let state:RouterStateSnapshot;
  console.log('authGuard.canActivate='+authGuard.canActivate())
  expect(authGuard.canActivate()).toBe(true);
});

it('not be able to hit route when user is not logged in', () => {
  usersService.isLogin = false;
  expect(authGuard.canActivate()).toBe(false);
});
});